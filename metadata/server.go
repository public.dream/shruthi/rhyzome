package metadata

import (
	"context"
	"encoding/json"
	"log"
	"net"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	libvirt "github.com/libvirt/libvirt-go"
	libvirtxml "github.com/libvirt/libvirt-go-xml"
	"github.com/mostlygeek/arp"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/utils"
	"git.callpipe.com/entanglement.garden/rhyzome/vault"
)

var (
	server *http.Server
)

func ListenAndServe() {
	r := mux.NewRouter()
	r.Use(lookupRequester)
	r.HandleFunc("/", index)
	r.HandleFunc("/cloud-init/user-data", cloudInitUserData)
	r.HandleFunc("/cloud-init/meta-data", cloudInitMetaData)
	r.HandleFunc("/guest-info", guestInfo)
	r.HandleFunc("/guest-get-osinfo", guestGetOSInfo)
	r.HandleFunc("/vault/inject-app-role", vault.InjectAppRole)
	r.HandleFunc("/vault/inject-cert", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		err := vault.InjectCert(ctx.Value(utils.InstanceDomainContextKey).(*libvirt.Domain))
		if err != nil {
			log.Println("Error injecting cert", err)
			http.Error(w, err.Error(), 500)
		}
	})

	configureInstanceInfo(r.PathPrefix("/instance-info/v1").Subrouter())

	server = &http.Server{
		Addr:    config.C.MetadataBind,
		Handler: utils.LoggingMiddleware(r),
	}
	log.Println("Starting metadata server on", server.Addr)
	err := server.ListenAndServe()
	if err != http.ErrServerClosed {
		panic(err)
	}
}

func lookupRequester(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conn, err := libvirt.NewConnect("qemu:///system")
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		defer conn.Close()

		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			http.Error(w, err.Error(), 404)
			return
		}

		mac := strings.Trim(arp.Search(ip), "'")

		log.Printf("[%s] Looking up metadata for %s", ip, mac)

		domainIDs, err := conn.ListDomains()
		if err != nil {
			http.Error(w, err.Error(), 404)
			return
		}

		ctx := r.Context()
		found := false

		for _, domainID := range domainIDs {
			domain, err := conn.LookupDomainById(domainID)
			if err != nil {
				continue
			}
			if domain == nil {
				log.Printf("LookupDomainById(%d) returned nil", domainID)
				continue
			}

			xmldoc, err := domain.GetXMLDesc(0)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}

			domainXML := libvirtxml.Domain{}
			if err := domainXML.Unmarshal(xmldoc); err != nil {
				continue
			}

			for _, iface := range domainXML.Devices.Interfaces {
				if iface.MAC.Address == mac {
					ctx = context.WithValue(ctx, utils.LibvirtConnecKey, conn)
					ctx = context.WithValue(ctx, utils.InstanceDomainContextKey, domain)
					ctx = context.WithValue(ctx, utils.InstanceDomainXMLContextKey, domainXML)
					found = true
					break
				}
			}
			if found {
				break
			}
		}

		if !found {
			http.Error(w, "unknown client", 401)
			return
		}

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func index(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	domain := ctx.Value(utils.InstanceDomainXMLContextKey).(libvirtxml.Domain)
	json.NewEncoder(w).Encode(domain)
}

func Shutdown() error {
	log.Println("Shutting down metadata server")
	return server.Shutdown(context.Background())
}
