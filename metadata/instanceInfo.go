package metadata

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	libvirt "github.com/libvirt/libvirt-go"
	libvirtxml "github.com/libvirt/libvirt-go-xml"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/utils"
)

type instanceInfoQuery func(libvirtxml.Domain) (string, error)

func guestInfo(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	domain := ctx.Value(utils.InstanceDomainContextKey).(*libvirt.Domain)
	info, err := domain.QemuAgentCommand("{\"execute\":\"guest-info\"}", 1, 0)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(info))
}

func guestGetOSInfo(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	domain := ctx.Value(utils.InstanceDomainContextKey).(*libvirt.Domain)
	info, err := domain.QemuAgentCommand("{\"execute\":\"guest-get-osinfo\"}", 1, 0)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(info))
}

func instanceInfoResponse(query instanceInfoQuery) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		domain := ctx.Value(utils.InstanceDomainXMLContextKey).(libvirtxml.Domain)
		result, err := query(domain)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		log.Println("instance info query response:", result)
		w.Write([]byte(fmt.Sprintf("%s\n", result)))
	}
}

func configureInstanceInfo(r *mux.Router) {
	r.HandleFunc("/hostname", instanceInfoResponse(func(d libvirtxml.Domain) (string, error) {
		return d.Name, nil
	}))

	r.HandleFunc("/fqdn", instanceInfoResponse(func(d libvirtxml.Domain) (string, error) {
		return fmt.Sprintf("%s.%s", d.Name, config.C.Hostname), nil
	}))

	r.HandleFunc("/id", instanceInfoResponse(func(d libvirtxml.Domain) (string, error) {
		return d.UUID, nil
	}))
}
