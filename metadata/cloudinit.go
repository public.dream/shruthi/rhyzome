package metadata

import (
	"log"
	"net/http"

	libvirtxml "github.com/libvirt/libvirt-go-xml"
	"gopkg.in/yaml.v2"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/utils"
)

type CloudInitMetadata struct {
	LocalHostname string `yaml:"local-hostname,omitempty"`
	InstanceID    string `yaml:"instance-id,omitempty"`
	CloudName     string `yaml:"cloud-name,omitempty"`
}

func cloudInitUserData(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	domain := ctx.Value(utils.InstanceDomainXMLContextKey).(libvirtxml.Domain)
	w.Write([]byte(domain.Description))
}

func cloudInitMetaData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/yaml")
	type cloudInitMetaDataResponse struct {
	}
	ctx := r.Context()
	domain := ctx.Value(utils.InstanceDomainXMLContextKey).(libvirtxml.Domain)
	err := yaml.NewEncoder(w).Encode(CloudInitMetadata{
		LocalHostname: domain.Name,
		InstanceID:    domain.UUID,
		CloudName:     config.C.CloudInit.CloudName,
	})
	if err != nil {
		log.Println("Error encoding response", err)
		http.Error(w, err.Error(), 500)
	}
}
