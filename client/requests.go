package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

func Get(server string, path string, out interface{}) error {
	return Request("GET", server+path, nil, out)
}

func Post(server string, path string, body interface{}, out interface{}) error {
	rawbody, err := json.Marshal(body)
	if err != nil {
		return err
	}
	return Request("POST", server+path, bytes.NewBuffer(rawbody), out)
}

func Request(method string, url string, body io.Reader, out interface{}) error {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil
	}

	if resp.Body == nil {
		return errors.New("response body is nil")
	}

	err = json.NewDecoder(resp.Body).Decode(out)
	if err != nil {
		return err
	}
	return nil
}
