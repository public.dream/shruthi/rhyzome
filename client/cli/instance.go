package cli

import (
	"github.com/spf13/cobra"
)

var instanceCmd = &cobra.Command{
	Use:   "instance",
	Short: "Commands to access instances",
	Long:  `...`,
	Run:   InstanceList,
}

func init() {
	RootCmd.AddCommand(instanceCmd)
}
