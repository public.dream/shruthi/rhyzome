package cli

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var (
	RootCmd = &cobra.Command{
		Use:   "rhyzomectl",
		Short: "Interact with Rhyzome",
		Long: `Rhyzome is a free/libre cloud computing platform, aimed at being less
		shitty than the others. So far it has not succeeded.

		rhyzomectl can be used to interact with rhyzome servers. Wouldn't it be nice
		if there was some documentation here? When you figure out whatever it is you
		came here to figure out, maybe you should update this string!`,
		PreRunE: checkRequiredFlags,
	}
	server string
)

func init() {
	RootCmd.PersistentFlags().StringVarP(&server, "server", "s", "", "The remote rhyzome host to use")
	RootCmd.MarkFlagRequired("server")
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
