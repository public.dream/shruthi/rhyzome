package cli

import (
	"log"
	"time"

	"git.callpipe.com/entanglement.garden/rhyzome/client"
	"git.callpipe.com/entanglement.garden/rhyzome/jobs"
)

func WaitForJob(id string) {
	printedEvents := 0

	for {
		var j jobs.Job
		client.Get(server, "/api/v1alpha1/jobs/"+id, &j)
		if len(j.Events) > printedEvents {
			for _, event := range j.Events[printedEvents:] {
				log.Println(event)
				printedEvents++
			}
		}

		if j.Completed {
			break
		}
		time.Sleep(time.Second)
	}
}
