package cli

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome/client"
	"git.callpipe.com/entanglement.garden/rhyzome/instances"
)

var listInstanceCommand = &cobra.Command{
	Use:   "list",
	Short: "List all instances on this server",
	Long:  `...`,
	Run:   InstanceList,
}

func init() {
	instanceCmd.AddCommand(listInstanceCommand)
}

func InstanceList(cmd *cobra.Command, args []string) {
	var allInstances []instances.Instance

	err := client.Get(server, "/api/v1alpha1/instances", &allInstances)
	if err != nil {
		log.Println("Error fetching instance list from ", server)
		panic(err)
	}
	for _, i := range allInstances {
		fmt.Println(i.Name)
	}
}
