package cli

import (
	"errors"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

// Adapted from https://github.com/spf13/cobra/issues/206#issuecomment-310848302
func checkRequiredFlags(cmd *cobra.Command, args []string) error {
	requiredError := false
	flagName := ""

	cmd.Flags().VisitAll(func(flag *pflag.Flag) {
		requiredAnnotation := flag.Annotations[cobra.BashCompOneRequiredFlag]
		if len(requiredAnnotation) == 0 {
			return
		}

		flagRequired := requiredAnnotation[0] == "true"

		if flagRequired && !flag.Changed {
			requiredError = true
			flagName = flag.Name
		}
	})

	if requiredError {
		return errors.New("Required flag `" + flagName + "` has not been set")
	}

	return nil
}
