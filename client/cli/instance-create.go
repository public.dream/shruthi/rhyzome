package cli

import (
	"github.com/spf13/cobra"

	"git.callpipe.com/entanglement.garden/rhyzome/client"
	"git.callpipe.com/entanglement.garden/rhyzome/instances"
	"git.callpipe.com/entanglement.garden/rhyzome/jobs"
)

var (
	createInstanceCommand = &cobra.Command{
		Use:     "create",
		Short:   "Create a new instance",
		Long:    `...`,
		Run:     createInstance,
		PreRunE: checkRequiredFlags,
	}

	name         string
	baseImage    string
	kernel       string
	kernelParams string
	autostart    bool
	memoryGB     uint
	cores        int
	diskSizeGB   uint64
)

func init() {
	createInstanceCommand.Flags().StringVarP(&name, "name", "n", "", "The name of the new instance")
	createInstanceCommand.Flags().StringVarP(&baseImage, "base-image", "i", "", "The image to use")
	createInstanceCommand.Flags().StringVarP(&kernel, "kernel", "k", "", "The kernel to use")
	createInstanceCommand.Flags().StringVarP(&kernelParams, "kernel-parameters", "p", "", "The paramters to pass to the kernel at boot time.")
	createInstanceCommand.Flags().Uint64VarP(&diskSizeGB, "disk", "d", 0, "The initial size of the instance's disk")
	createInstanceCommand.Flags().BoolVarP(&autostart, "autostart", "a", true, "whether to start this instance on host boot")
	createInstanceCommand.Flags().UintVarP(&memoryGB, "memory", "m", 0, "The amount of memory to assign to the instance, in GB")
	createInstanceCommand.Flags().IntVarP(&cores, "cores", "c", 0, "The number of cores to assign to the new instance")

	createInstanceCommand.MarkFlagRequired("name")
	instanceCmd.AddCommand(createInstanceCommand)
}

func createInstance(cmd *cobra.Command, args []string) {
	instance := instances.Instance{
		Name:         name,
		BaseImage:    baseImage,
		Autostart:    autostart,
		Kernel:       kernel,
		KernelParams: kernelParams,
	}

	if diskSizeGB > 0 {
		instance.DiskSizeGB = diskSizeGB
	}

	if memoryGB > 0 {
		instance.MemoryGB = memoryGB
	}

	if cores > 0 {
		instance.Cores = cores
	}
	var job jobs.Job
	client.Post(server, "/api/v1alpha1/instances", instance, &job)
	WaitForJob(job.ID)
}
