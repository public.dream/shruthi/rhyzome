package jobs

import (
	"crypto/rand"
	"encoding/base64"
	"log"
)

const (
	generatedIDSize = 10
)

// JobFunction is a function to be run asyncronosly as a job
type JobFunction func(*Job) error

var (
	jobs = make(map[string]*Job)
)

// Job is a task being run asyncronosly
type Job struct {
	ID        string
	Name      string
	Events    []string
	Status    string
	Completed bool
	Error     error
	Fn        JobFunction `json:"-"`
}

func NewJob(name string, fn JobFunction) (j *Job) {
	j = &Job{
		Name: name,
		Fn:   fn,
	}
	j.GenerateID()
	go j.Run()
	return j
}

func All() []*Job {
	var jobSlice []*Job
	for _, job := range jobs {
		jobSlice = append(jobSlice, job)
	}
	return jobSlice
}

func Get(ID string) *Job {
	return jobs[ID]
}

func (j *Job) GenerateID() {
	if j.ID != "" {
		return
	}

	b := make([]byte, generatedIDSize)
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}

	id := base64.URLEncoding.EncodeToString(b)
	if _, ok := jobs[id]; !ok {
		j.ID = id
		jobs[id] = j
		log.Println("New job: ", j.ID)
	} else {
		j.GenerateID()
	}
}

func (j *Job) Run() {
	j.GenerateID()
	log.Printf("Running job %s (%s)\n", j.Name, j.ID)
	j.Completed = false
	j.SetStatus("started")
	j.Error = j.Fn(j)
	j.Completed = true
	if j.Error != nil {
		j.SetStatus(j.Error.Error())
	} else {
		j.SetStatus("completed")
	}
}

func (j *Job) SetStatus(status string) {
	j.Status = status
	j.Events = append(j.Events, status)
	log.Printf("[job %s] status: %s\n", j.ID, status)
}
