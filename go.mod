module git.callpipe.com/entanglement.garden/rhyzome

go 1.14

require (
	git.callpipe.com/entanglement.garden/qapi v0.0.0-20200213040443-8ee80086af91
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/vault/api v1.0.5-0.20200501215127-6f01f6369d01
	github.com/libvirt/libvirt-go v6.1.0+incompatible
	github.com/libvirt/libvirt-go-xml v6.2.0+incompatible
	github.com/mostlygeek/arp v0.0.0-20170424181311-541a2129847a
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
	gopkg.in/yaml.v2 v2.2.8
)
