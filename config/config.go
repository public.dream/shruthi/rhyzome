package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"github.com/hashicorp/vault/api"
	libvirt "github.com/libvirt/libvirt-go"
)

var ConfigFiles = []string{"/etc/rhyzome.conf", "rhyzome.conf"}

// Config describes all configurable keys
type Config struct {
	Bind                string                    // Bind is the address (and port) to bind the REST server to
	BridgeInterface     string                    // BridgeInterface is the bridge that all network interfaces are added to
	DiskStoragePool     string                    // DiskStoragePool is the name of the storage pool to use
	Hostname            string                    // Hostname is the domain that guests will be created under
	ImageDir            string                    // ImageDir is the path to the local image pool
	ImageGroup          string                    // ImageGroup is the GID that should own volume images
	ImageHost           string                    // ImageHost is the base URL for the disk image server
	ImageOwner          string                    // ImageOwner is the UID that should own volume images
	MetadataBind        string                    // MetadataBind is the port (and optionally IP) to bind the metadata server to
	CloudInit           CloudInitConfig           // CloudInit contains settings that are specific to the cloud-init integration
	Vault               VaultConfig               // Vault contains settings for connecting to the vault server
	CredentialInjection CredentialInjectionConfig // CredentialInjection configures how credentials are injected into guests
}

type VaultConfig struct {
	TLSConfig             api.TLSConfig // TLSConfig is the TLS configuration to use, if any
	Address               string
	RoleIDFilePath        string
	SecretIDFilePath      string
	InstancePKIRoleParams map[string]interface{}
}

type CloudInitConfig struct {
	MetadataURL string // MetadataURL is the base URL for the metadata server. If unsure, set it to http://<rhyzome-server-ip-address>:8081
	CloudName   string // CloudName is name of the cloud we tell cloud-init it's running on
}

type CredentialInjectionConfig struct {
	CommandPollMS              int
	GuestCommandTimeoutSeconds libvirt.DomainQemuAgentCommandTimeout
	GuestAuthFolder            string
	GuestCertFolder            string
	SecretIDTTL                string
	TokenPolicies              []string
	SecretIDNumUses            string
	TokenMaxTTL                string
}

var (
	// C stores the actual configured values
	C Config

	// Defaults are the default values for each config option, set in the Load() function
	Defaults = Config{
		Bind:            ":8080",
		BridgeInterface: "brlan",
		DiskStoragePool: "default",
		ImageDir:        "/var/lib/libvirt/images",
		ImageGroup:      "64055",
		ImageHost:       "http://image-host.fruit-0.entanglement.garden",
		ImageOwner:      "64055",
		MetadataBind:    "127.0.0.1:8081",
		CloudInit: CloudInitConfig{
			MetadataURL: "http://127.0.0.1:80801/",
			CloudName:   "rhyzome",
		},
		Vault: VaultConfig{
			TLSConfig: api.TLSConfig{},
			InstancePKIRoleParams: map[string]interface{}{
				"allow_localhost":    false,
				"allow_bare_domains": true,
			},
		},
		CredentialInjection: CredentialInjectionConfig{
			GuestCommandTimeoutSeconds: 5,
			GuestAuthFolder:            "/etc/entanglement/auth",
			GuestCertFolder:            "/etc/entanglement/pki",
			CommandPollMS:              250,
			SecretIDTTL:                "1h",
			SecretIDNumUses:            "1",
			TokenMaxTTL:                "24h",
		},
	}
)

// Load loads the configuration off the disk
func Load() {
	C = Defaults
	for _, filename := range ConfigFiles {
		jsonFile, err := os.Open(filename)
		if err != nil {
			if !os.IsNotExist(err) {
				log.Printf("Error opening config file %s: %s", filename, err.Error())
			}
			continue
		}
		defer jsonFile.Close()

		byteValue, err := ioutil.ReadAll(jsonFile)
		if err != nil {
			log.Printf("Error reading config file %s: %s", filename, err.Error())
			continue
		}

		json.Unmarshal(byteValue, &C)
		log.Println("Successfully read config from", filename)
		log.Printf("%+v", C)
	}

	if C.Hostname == "" {
		log.Println("Hostname not specified in config, setting from system")
		hostname, err := os.Hostname()
		if err != nil {
			log.Println("Error getting hostname from system", err)
		} else {
			C.Hostname = hostname
			log.Println("Set hostname to", C.Hostname)
		}
	}
}
