package vault

import (
	"fmt"
	"log"

	libvirt "github.com/libvirt/libvirt-go"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
)

// InjectCert generates a role, certificate and private key for a given VM and injects it into the VM.
// Eventually we should have the private key generated on the VM and just sign the CSR
func InjectCert(domain *libvirt.Domain) error {
	log.Println("generating key/cert and injecting")

	domainName, err := domain.GetName()
	if err != nil {
		log.Println("Error getting domain name")
		return err
	}

	_, err = GuestExec(domain, "mkdir", "-p", config.C.CredentialInjection.GuestCertFolder)
	if err != nil {
		log.Println("Error creating", config.C.CredentialInjection.GuestCertFolder, "on guest:", err)
		return err
	}

	// Create the cert role
	fqdn := fmt.Sprintf("%s.%s", domainName, config.C.Hostname)
	_, err = vaultClient.Logical().Write(fmt.Sprintf("pki/roles/%s", fqdn), map[string]interface{}{
		"allowed_domains":    fqdn,
		"allow_bare_domains": true,
	})
	if err != nil {
		log.Println("Error creating vault cert", fqdn, err)
		return err
	}

	cert, err := vaultClient.Logical().Write(fmt.Sprintf("pki/issue/%s", fqdn), map[string]interface{}{"common_name": fqdn})
	if err != nil {
		log.Println("Error issuing certificate for ", fqdn, err)
		return err
	}

	certificate, ok := cert.Data["certificate"].(string)
	if !ok {
		log.Println("Warning: no certificate in response")
	}
	WriteSecret(domain, fmt.Sprintf("%s/cert.pem", config.C.CredentialInjection.GuestCertFolder), certificate)

	key, ok := cert.Data["private_key"].(string)
	if !ok {
		log.Println("Warning: no private_key in response")
	}
	WriteSecret(domain, fmt.Sprintf("%s/key.pem", config.C.CredentialInjection.GuestCertFolder), key)

	ca := cert.Data["issuing_ca"].(string)
	if !ok {
		log.Println("Warning: no issuing_ca in response")
	}
	WriteSecret(domain, fmt.Sprintf("%s/ca.pem", config.C.CredentialInjection.GuestCertFolder), ca)

	return nil
}
