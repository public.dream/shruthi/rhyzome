package vault

import (
	"github.com/hashicorp/vault/api"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
)

var (
	vaultClient  *api.Client
	tokenWatcher *api.LifetimeWatcher
	secret       *api.Secret
)

// Load initializes the vault client with the configured vaults from the config file and enviornment variables
func Load() error {
	c := api.DefaultConfig()
	c.Address = config.C.Vault.Address

	err := c.ConfigureTLS(&config.C.Vault.TLSConfig)
	if err != nil {
		log.Println("Error configuring Vault TLS")
		return err
	}

	err = c.ReadEnvironment()
	if err != nil {
		log.Println("Error configuring Vault from enviornment")
		return err
	}

	vaultClient, err = api.NewClient(c)
	if err != nil {
		log.Println("Error configuring vault client")
		return err
	}

	roleID, err := ioutil.ReadFile(config.C.Vault.RoleIDFilePath)
	if err != nil {
		log.Println("Error reading role id:", err)
		return err
	}

	secretID, err := ioutil.ReadFile(config.C.Vault.SecretIDFilePath)
	if err != nil {
		log.Println("Error reading secret id:", err)
		return err
	}

	log.Println("Logging into vault at", c.Address)
	secret, err = vaultClient.Logical().Write("/auth/approle/login", map[string]interface{}{
		"role_id":   strings.TrimSpace(string(roleID)),
		"secret_id": strings.TrimSpace(string(secretID)),
	})
	if err != nil {
		log.Println("Error logging into vault:", err)
		return err
	}

	err = startTokenWatcher()

	return err
}

func startTokenWatcher() (err error) {
	log.Println("Starting token watcher")
	if vaultClient == nil {
		log.Println("vaultClient is nil! We will now crash")
	}
	tokenWatcher, err = vaultClient.NewLifetimeWatcher(&api.LifetimeWatcherInput{Secret: secret})
	if err != nil {
		return err
	}
	go tokenWatcher.Start()
	go watchToken()
	return nil
}

func watchToken() {
	if tokenWatcher == nil {
		log.Println("WatchToken gonna fail because tokenWatcher is nil")
	}
	for {
		select {
		case err := <-tokenWatcher.DoneCh():
			if err != nil {
				log.Println("Error in vault watcher:", err)
				time.Sleep(5 * time.Second)
				tokenWatcher.Stop()
				startTokenWatcher()
				return
			}

		case renewal := <-tokenWatcher.RenewCh():
			log.Printf("Successfully renewed vault client token %+v", renewal.Secret.Auth)
			vaultClient.SetToken(renewal.Secret.Auth.ClientToken)
		}
	}
}
