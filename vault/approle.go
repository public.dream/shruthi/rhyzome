package vault

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	libvirt "github.com/libvirt/libvirt-go"

	"git.callpipe.com/entanglement.garden/qapi"
	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/utils"
)

// InjectAppRole injects the credentials for a given AppRole
func InjectAppRole(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	domain := ctx.Value(utils.InstanceDomainContextKey).(*libvirt.Domain)

	domainName, err := domain.GetName()
	if err != nil {
		log.Println("Error getting domain name")
		http.Error(w, err.Error(), 500)
		return
	}

	_, err = GuestExec(domain, "mkdir", "-p", config.C.CredentialInjection.GuestAuthFolder)
	if err != nil {
		log.Println("Error creating", config.C.CredentialInjection.GuestAuthFolder, "on guest:", err)
		http.Error(w, err.Error(), 500)
		return
	}

	// Create the approle
	appRolePath := fmt.Sprintf("auth/approle/role/%s.%s", domainName, config.C.Hostname)
	_, err = vaultClient.Logical().Write(appRolePath, map[string]interface{}{
		"secret_id_ttl":      config.C.CredentialInjection.SecretIDTTL,
		"token_policies":     strings.Join(config.C.CredentialInjection.TokenPolicies, ","),
		"secret_id_num_uses": config.C.CredentialInjection.SecretIDNumUses,
		"token_max_ttl":      config.C.CredentialInjection.TokenMaxTTL,
	})
	if err != nil {
		log.Println("Error creating vault role", appRolePath, err)
		http.Error(w, err.Error(), 500)
		return
	}

	roleID, err := vaultClient.Logical().Read(fmt.Sprintf("%s/role-id", appRolePath))
	if err != nil {
		log.Println("Error getting role-id for role", appRolePath)
		http.Error(w, err.Error(), 500)
		return
	}

	roleIDstring, ok := roleID.Data["role_id"].(string)
	if !ok {
		msg := "Cannot convert role_id as received from vault to string"
		log.Println(msg)
		http.Error(w, msg, 500)
		return
	}

	fqdn := fmt.Sprintf("%s.%s", domainName, config.C.Hostname)
	params := config.C.Vault.InstancePKIRoleParams
	params["allowed_domains"] = []string{fqdn}
	log.Printf("Generating role %s: %+v", fqdn, params)
	_, err = vaultClient.Logical().Write(fmt.Sprintf("/pki/roles/%s", fqdn), params)
	if err != nil {
		log.Println("Error creating role", fqdn, err)
		http.Error(w, err.Error(), 500)
		return
	}

	err = WriteSecret(domain, fmt.Sprintf("%s/role-id", config.C.CredentialInjection.GuestAuthFolder), roleIDstring)
	if err != nil {
		log.Println("Error writing role-id file to guest", err)
		http.Error(w, err.Error(), 500)
		return
	}

	metadata := map[string]string{"fqdn": fqdn}
	metadataString, err := json.Marshal(metadata)
	if err != nil {
		log.Println("Error generating metadata string to create secret-id", err)
		http.Error(w, err.Error(), 500)
		return
	}

	secretID, err := vaultClient.Logical().Write(fmt.Sprintf("%s/secret-id", appRolePath), map[string]interface{}{"metadata": metadataString})
	if err != nil {
		log.Println("Error getting secret-id for role", appRolePath, err)
		http.Error(w, err.Error(), 500)
		return
	}

	secretIDstring, ok := secretID.Data["secret_id"].(string)
	if !ok {
		msg := "Cannot convert secret_id as received from vault to string"
		log.Println(msg)
		http.Error(w, msg, 500)
		return
	}

	err = WriteSecret(domain, fmt.Sprintf("%s/secret-id", config.C.CredentialInjection.GuestAuthFolder), secretIDstring)
	if err != nil {
		log.Println("Error writing secret-id file to guest", err)
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.NewEncoder(w).Encode(map[string]string{"status": "success"})
	if err != nil {
		log.Println("Error encoding response", err)
		http.Error(w, err.Error(), 500)
	}
}

func WriteSecret(domain *libvirt.Domain, path string, secret string) error {
	fh, err := qapi.ExecuteGuestFileOpen(domain, qapi.GuestFileOpenArg{Path: path, Mode: "w"})
	if err != nil {
		log.Println("Error opening", path, "on guest", err)
		return err
	}

	_, err = qapi.ExecuteGuestFileWrite(domain, qapi.GuestFileWriteArg{
		Handle: fh,
		BufB64: base64.StdEncoding.EncodeToString([]byte(secret)),
	})
	if err != nil {
		log.Println("Error writing to", path, "on guest", err)
		return err
	}

	err = qapi.ExecuteGuestFileClose(domain, qapi.GuestFileCloseArg{Handle: fh})
	if err != nil {
		log.Println("Error closing file", path, "on guest after writing secret", err)
		return err
	}

	return nil
}

// GuestExec runs a command on the specified domain as root (via the qemu guest agent), waits for it to complete, and returns the response
// TODO: Should this be a member function of instances.Instance?
func GuestExec(domain *libvirt.Domain, path string, args ...string) (response qapi.GuestExecStatus, err error) {
	commandResponse, err := qapi.ExecuteGuestExec(domain, qapi.GuestExecArg{
		Path:          path,
		Arg:           args,
		CaptureOutput: true,
	})
	if err != nil {
		log.Println("Error executing guest-exec command:", err)
		return
	}

	for {
		response, err = qapi.ExecuteGuestExecStatus(domain, qapi.GuestExecStatusArg{PID: commandResponse.PID})
		if err != nil {
			log.Printf("Error checking status of command running on guest: %s", err.Error())
			return
		}

		if response.Exited {
			log.Printf("Command %s exited: %+v", path, response)
			break
		}

		time.Sleep(time.Duration(config.C.CredentialInjection.CommandPollMS) * time.Millisecond)
	}

	return
}
