# Rhyzome

*A provisioner built upon libvirt*

> A DIY EC2 ⸺ Finn Herzfeld

Rhyzome runs on a host and exposes an API (currently RESTful) to perform CRUD operations on instances (virtual machines).

## Vocabulary

* Instance

*This abstraction is how we refer to a system which can be manipulated by Rhyzome. An instance could be a Virtual Machine (VM), Unikernel, or perhaps something like a container.* 

## Requirements

### Runtime
* `libvirt-daemon >=5.0.0`

### Buildtime
* `Go >=1.11`
* `libvirt-headers >=5.0.0`

## Building
```shell
# Build the daemon/server
go build -v -o rhyzomesrv cmd/rhyzome/main.go

# Build the client
go build -v -o rhyzomectl cmd/rhyzomectl/main.go
```

## Usage

### Running the Daemon

As a pre-requisite, the user running the daemon needs to be in the `libvirt` group.

```shell
./rhyzomesrv
```


#### Configuration

Rhyzome will look for a configuration file named `rhyzome.conf` in the current directory. It will use default settings if no configuration file is provided.

Example config:

```json
{
	"DiskStoragePool": "default",
	"Hostname": "localhost",
	"Bind": ":8080",
	"MetadataBind": ":8081",
	"ImageHost": "http://172.18.10.3",
	"CloudInit": {
		"MetadataURL": "http://172.18.10.22:8081"
	},
}
```


### Creating a New Instance

```shell
./rhyzomectl --server http://172.18.10.22:8080 instance create \
--name mumble-server \
--base-image debian
```

### Creating a Direct-Kernel-Boot Instance

```shell
./rhyzomectl --server http://172.18.10.22:8080 instance create \
--name https-server \
--kernel /path/to/kernel/https.virtio \
--kernel-parameters "--ipv4-gateway 192.168.100.1 --ipv4 192.168.100.43/24"
```

## Future

Proposed eventual architecture:
* Rhyzome Core (*does not exist yet*) - A central server that communicates with the Rhyzome Agent (this repo), and provides a RESTful interface for clients to interact with it. The agent no longer exposes the client-side API. 

