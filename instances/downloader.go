package instances

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	libvirt "github.com/libvirt/libvirt-go"
	"github.com/xi2/xz"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
)

func downloadToVolume(image string, volume *libvirt.StorageVol, conn *libvirt.Connect) error {
	hashes, err := getHashes(image)
	if err != nil {
		return err
	}

	filename := fmt.Sprintf("%s.qcow2.xz", image)
	url := fmt.Sprintf("%s/%s/%s", config.C.ImageHost, image, filename)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	hash := sha256.New()

	uncompressor, err := xz.NewReader(io.TeeReader(resp.Body, hash), 0)
	if err != nil {
		return err
	}

	stream, err := conn.NewStream(0)
	if err != nil {
		return err
	}

	defer func() {
		// Frees up the stream, if needed
		err = stream.Free()
		if err != nil {
			return
		}
	}()

	err = volume.Upload(stream, 0, 0, 0)
	if err != nil {
		return err
	}

	err = stream.SendAll(func(s *libvirt.Stream, i int) ([]byte, error) {
		buf := make([]byte, i)
		_, err := uncompressor.Read(buf)
		if err == io.EOF {
			// Go's io interface raise an io.EOF error to indicate the end of the stream,
			// but libvirt indicates EOF with a zero-length response
			return make([]byte, 0), nil
		}
		if err != nil {
			log.Println("Error while reading buffer", err.Error())
			return buf, err
		}
		return buf, nil
	})

	if err != nil {
		return err
	}

	hashstr := hex.EncodeToString(hash.Sum(nil))

	if hashstr != hashes[filename] {
		return fmt.Errorf("Got bad hash when downloading %s: got %s expected %s", url, hashstr, hashes[filename])
	}
	return nil
}

func getHashes(image string) (map[string]string, error) {
	url := fmt.Sprintf("%s/%s/SHA256SUMS", config.C.ImageHost, image)

	resp, err := http.Get(url)
	if err != nil {
		return map[string]string{}, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return map[string]string{}, err
	}

	hashes := make(map[string]string)

	for i, line := range strings.Split(string(body), "\n") {
		if line == "" {
			continue
		}
		lineparts := strings.Split(line, "  ")
		if len(lineparts) != 2 {
			log.Printf("Unable to parse line %d", i)
			continue
		}
		hashes[lineparts[1]] = lineparts[0]
	}
	return hashes, nil
}
