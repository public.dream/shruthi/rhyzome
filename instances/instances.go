package instances

import (
	"fmt"
	"log"

	libvirt "github.com/libvirt/libvirt-go"
	libvirtxml "github.com/libvirt/libvirt-go-xml"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
)

type contextKey int

// Instance is a VM and it's affiliated resources
type Instance struct {
	Name         string `json:",omitempty"`
	UUID         string `json:",omitempty"`
	BaseImage    string `json:",omitempty"`
	Autostart    bool   `json:",omitempty"`
	Active       bool   `json:",omitempty"`
	DomainID     uint   `json:",omitempty"`
	MemoryGB     uint   `json:",omitempty"`
	Cores        int    `json:",omitempty"`
	DiskSizeGB   uint64 `json:",omitempty"`
	Kernel       string `json:",omitempty"`
	KernelParams string `json:",omitempty"`
	domain       *libvirt.Domain
}

// GetDefaultInstance is the defaults for new instances
func GetDefaultInstance() Instance {
	return Instance{
		Autostart:  true,
		MemoryGB:   4,
		DiskSizeGB: 20,
		Cores:      2,
	}
}

func GetAll() (instances []Instance, err error) {
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return []Instance{}, err
	}
	defer conn.Close()

	domainIDs, err := conn.ListDomains()
	if err != nil {
		return
	}

	for _, domainID := range domainIDs {
		i, err := FromDomainIDWithConnect(conn, domainID)
		if err != nil {
			log.Printf("Error getting instance from DomainID %d: %s", domainID, err.Error())
			continue
		}
		instances = append(instances, i)
	}
	return
}

func FromDomain(dom *libvirt.Domain) (i Instance, err error) {
	if dom == nil {
		return Instance{}, fmt.Errorf("domain is nil")
	}

	i.domain = dom

	i.Autostart, err = dom.GetAutostart()
	if err != nil {
		return
	}

	i.Name, err = dom.GetName()
	if err != nil {
		return
	}

	i.UUID, err = dom.GetUUIDString()
	if err != nil {
		return
	}

	i.DomainID, err = dom.GetID()
	if err != nil {
		return
	}

	i.Active, err = dom.IsActive()

	return
}

func FromDomainID(domainID uint32) (Instance, error) {
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return Instance{}, err
	}
	defer conn.Close()
	return FromDomainIDWithConnect(conn, domainID)
}

func FromDomainIDWithConnect(conn *libvirt.Connect, domainID uint32) (Instance, error) {
	dom, err := conn.LookupDomainById(domainID)
	if err != nil {
		return Instance{}, err
	}

	if dom == nil {
		err = fmt.Errorf("LookupDomainById(%d) returned nil", domainID)
		return Instance{}, err
	}

	return FromDomain(dom)
}

func FromDomainUUID(domainUUID string) (Instance, error) {
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return Instance{}, err
	}
	defer conn.Close()

	dom, err := conn.LookupDomainByUUIDString(domainUUID)
	if err != nil {
		return Instance{}, err
	}
	if dom == nil {
		err = fmt.Errorf("LookupDomainByUUID(%d) returned nil", domainUUID)
		return Instance{}, err
	}

	return FromDomain(dom)
}

// GetVolumeXML generates libvirt XML that describes the instance volume
func (i *Instance) GetVolumeXML() (string, error) {
	volume := &libvirtxml.StorageVolume{
		Name: fmt.Sprintf("%s.qcow2", i.Name),
		Capacity: &libvirtxml.StorageVolumeSize{
			Unit:  "GB",
			Value: i.DiskSizeGB,
		},
		Target: &libvirtxml.StorageVolumeTarget{
			Format: &libvirtxml.StorageVolumeTargetFormat{Type: "qcow2"},
			Permissions: &libvirtxml.StorageVolumeTargetPermissions{
				Mode:  "0644",
				Owner: config.C.ImageOwner,
				Group: config.C.ImageGroup,
			},
		},
	}

	return volume.Marshal()
}

// GetXML returns the libvirt XML of the underlying domain
func (i *Instance) GetXML() (string, error) {
	if i.domain == nil {
		return "", fmt.Errorf("unable to GetXML from nil domain")
	}
	return i.domain.GetXMLDesc(1)
}
