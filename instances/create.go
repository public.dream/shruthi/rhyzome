package instances

import (
	"fmt"
	"log"

	libvirt "github.com/libvirt/libvirt-go"
	libvirtxml "github.com/libvirt/libvirt-go-xml"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/instances/storage"
	"git.callpipe.com/entanglement.garden/rhyzome/jobs"
)

func (i *Instance) CreateAsJob(j *jobs.Job) error {
	log.Printf("[job %s] Creating instance: %#v", j.ID, i)

	j.SetStatus("Connecting to libvirt")
	conn, err := libvirt.NewConnect("qemu:///system")
	if err != nil {
		return err
	}
	defer conn.Close()

	if i.Kernel == "" {
		// Create volume for new instance
		j.SetStatus("Creating volume")
		storagePool, err := storage.GetPool(conn)
		if err != nil {
			return err
		}

		volume, err := storagePool.CreateVolume(i.Name, i.DiskSizeGB)
		if err != nil {
			return err
		}

		j.SetStatus("Downloading base image")
		err = downloadToVolume(i.BaseImage, volume, conn)
		if err != nil {
			volume.Delete(0) // If we put a 1 it will overwrite all the data from the disk, see https://libvirt.org/html/libvirt-libvirt-storage.html#virStorageVolDeleteFlags
			return err
		}

		j.SetStatus("Creating domain")
		if err != nil {
			return err
		}

		// Create a regular VM domain
		domainXML := &libvirtxml.Domain{
			Type:   "kvm",
			Name:   i.Name,
			Memory: &libvirtxml.DomainMemory{Value: i.MemoryGB, Unit: "GiB"},
			VCPU:   &libvirtxml.DomainVCPU{Value: i.Cores},
			OS: &libvirtxml.DomainOS{
				Type:        &libvirtxml.DomainOSType{Arch: "x86_64", Type: "hvm"},
				BootDevices: []libvirtxml.DomainBootDevice{libvirtxml.DomainBootDevice{Dev: "hd"}},
				Kernel:      i.Kernel,
				Cmdline:     i.KernelParams,
			},
			Features: &libvirtxml.DomainFeatureList{
				ACPI:   &libvirtxml.DomainFeature{},
				APIC:   &libvirtxml.DomainFeatureAPIC{},
				VMPort: &libvirtxml.DomainFeatureState{State: "off"},
			},
			CPU: &libvirtxml.DomainCPU{Mode: "host-model"},
			Devices: &libvirtxml.DomainDeviceList{
				Emulator: "/usr/bin/kvm",
				Disks:    []libvirtxml.DomainDisk{storagePool.GetDomainDiskXML(i.Name)},
				Interfaces: []libvirtxml.DomainInterface{
					libvirtxml.DomainInterface{
						Source: &libvirtxml.DomainInterfaceSource{Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface}},
						Model:  &libvirtxml.DomainInterfaceModel{Type: "virtio"},
					},
				},
				Channels: []libvirtxml.DomainChannel{libvirtxml.DomainChannel{
					Source: &libvirtxml.DomainChardevSource{UNIX: &libvirtxml.DomainChardevSourceUNIX{Path: "/var/lib/libvirt/qemu/f16x86_64.agent", Mode: "bind"}},
					Target: &libvirtxml.DomainChannelTarget{VirtIO: &libvirtxml.DomainChannelTargetVirtIO{Name: "org.qemu.guest_agent.0"}},
				}},
				Consoles: []libvirtxml.DomainConsole{libvirtxml.DomainConsole{Target: &libvirtxml.DomainConsoleTarget{}}},
				Serials:  []libvirtxml.DomainSerial{libvirtxml.DomainSerial{}},
			},
			QEMUCommandline: &libvirtxml.DomainQEMUCommandline{
				Args: []libvirtxml.DomainQEMUCommandlineArg{
					libvirtxml.DomainQEMUCommandlineArg{Value: "-smbios"},
					libvirtxml.DomainQEMUCommandlineArg{Value: fmt.Sprintf("type=1,serial=ds=nocloud-net;s=%s/cloud-init/", config.C.CloudInit.MetadataURL)},
				},
			},
		}

		domainXMLString, err := domainXML.Marshal()
		if err != nil {
			return err
		}

		domain, err := conn.DomainDefineXML(domainXMLString)
		if err != nil {
			return err
		}

		j.SetStatus("Booting domain")
		err = domain.Create()
		if err != nil {
			return err
		}

		j.SetStatus("Domain booted")
		return nil
	} else {

		j.SetStatus("Creating domain")
		if err != nil {
			return err
		}

		// Create a kernel-boot domain
		domainXML := &libvirtxml.Domain{
			Type:   "kvm",
			Name:   i.Name,
			Memory: &libvirtxml.DomainMemory{Value: i.MemoryGB, Unit: "GiB"},
			VCPU:   &libvirtxml.DomainVCPU{Value: i.Cores},
			OS: &libvirtxml.DomainOS{
				Type:        &libvirtxml.DomainOSType{Arch: "x86_64", Type: "hvm"},
				BootDevices: []libvirtxml.DomainBootDevice{libvirtxml.DomainBootDevice{Dev: "hd"}},
				Kernel:      i.Kernel,
				Cmdline:     i.KernelParams,
			},
			Features: &libvirtxml.DomainFeatureList{
				ACPI:   &libvirtxml.DomainFeature{},
				APIC:   &libvirtxml.DomainFeatureAPIC{},
				VMPort: &libvirtxml.DomainFeatureState{State: "off"},
			},
			CPU: &libvirtxml.DomainCPU{Mode: "host-model"},
			Devices: &libvirtxml.DomainDeviceList{
				Emulator: "/usr/bin/kvm",
				Interfaces: []libvirtxml.DomainInterface{
					libvirtxml.DomainInterface{
						Source: &libvirtxml.DomainInterfaceSource{Bridge: &libvirtxml.DomainInterfaceSourceBridge{Bridge: config.C.BridgeInterface}},
						Model:  &libvirtxml.DomainInterfaceModel{Type: "virtio"},
					},
				},
				Channels: []libvirtxml.DomainChannel{libvirtxml.DomainChannel{
					Source: &libvirtxml.DomainChardevSource{UNIX: &libvirtxml.DomainChardevSourceUNIX{Path: "/var/lib/libvirt/qemu/f16x86_64.agent", Mode: "bind"}},
					Target: &libvirtxml.DomainChannelTarget{VirtIO: &libvirtxml.DomainChannelTargetVirtIO{Name: "org.qemu.guest_agent.0"}},
				}},
				Consoles: []libvirtxml.DomainConsole{libvirtxml.DomainConsole{Target: &libvirtxml.DomainConsoleTarget{}}},
				Serials:  []libvirtxml.DomainSerial{libvirtxml.DomainSerial{}},
			},
			QEMUCommandline: &libvirtxml.DomainQEMUCommandline{
				Args: []libvirtxml.DomainQEMUCommandlineArg{
					libvirtxml.DomainQEMUCommandlineArg{Value: "-smbios"},
					libvirtxml.DomainQEMUCommandlineArg{Value: fmt.Sprintf("type=1,serial=ds=nocloud-net;s=%s/cloud-init/", config.C.CloudInit.MetadataURL)},
				},
			},
		}

		domainXMLString, err := domainXML.Marshal()
		if err != nil {
			return err
		}

		domain, err := conn.DomainDefineXML(domainXMLString)
		if err != nil {
			return err
		}

		j.SetStatus("Booting domain")
		err = domain.Create()
		if err != nil {
			return err
		}

		j.SetStatus("Domain booted")
		return nil
	}
}
