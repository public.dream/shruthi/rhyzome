package storage

import (
	"fmt"
	libvirt "github.com/libvirt/libvirt-go"
	libvirtxml "github.com/libvirt/libvirt-go-xml"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
)

// PoolType is an interface for the functionality around a type of StoragePool
type PoolType interface {
	CreateVolume(string, uint64) (*libvirt.StorageVol, error)
	GetVolumeName(string) string
	GetDomainDiskXML(string) libvirtxml.DomainDisk
}

// GetPool retrieves the configured Storage Pool from libvirt
func GetPool(conn *libvirt.Connect) (PoolType, error) {
	pool, err := conn.LookupStoragePoolByName(config.C.DiskStoragePool)
	if err != nil {
		return nil, err
	}

	xmldescription, err := pool.GetXMLDesc(0)
	if err != nil {
		return nil, err
	}

	p := libvirtxml.StoragePool{}
	err = p.Unmarshal(xmldescription)
	if err != nil {
		return nil, err
	}

	switch p.Type {
	case "dir":
		return NewDirPool(pool)
		break
	case "logical":
		return NewLogicalPool(pool)
		break
	}

	return nil, fmt.Errorf("Unknown pool type %s", p.Type)
}
