package storage

import (
	libvirt "github.com/libvirt/libvirt-go"
	libvirtxml "github.com/libvirt/libvirt-go-xml"
)

// LogicalPool is a StoragePool of type "dir"
type LogicalPool struct {
	pool *libvirt.StoragePool
	name string
}

func (p LogicalPool) CreateVolume(name string, sizeGB uint64) (*libvirt.StorageVol, error) {
	volumeXML := &libvirtxml.StorageVolume{
		Name: p.GetVolumeName(name),
		Capacity: &libvirtxml.StorageVolumeSize{
			Unit:  "GB",
			Value: sizeGB,
		},
	}

	xmlstr, err := volumeXML.Marshal()
	if err != nil {
		return nil, err
	}

	return p.pool.StorageVolCreateXML(xmlstr, 0)
}

func (p LogicalPool) GetVolumeName(name string) string {
	return name
}

func (p LogicalPool) GetDomainDiskXML(name string) libvirtxml.DomainDisk {
	return libvirtxml.DomainDisk{
		Device: "disk",
		Driver: &libvirtxml.DomainDiskDriver{Name: "qemu", Type: "raw"},
		Source: &libvirtxml.DomainDiskSource{
			Volume: &libvirtxml.DomainDiskSourceVolume{Pool: p.name, Volume: p.GetVolumeName(name)},
		},
		Target: &libvirtxml.DomainDiskTarget{Dev: "vda"},
	}
}

// NewLogicalPool creates a storage pool of type dir
func NewLogicalPool(pool *libvirt.StoragePool) (LogicalPool, error) {
	name, err := pool.GetName()
	if err != nil {
		return LogicalPool{}, err
	}

	return LogicalPool{pool: pool, name: name}, nil
}
