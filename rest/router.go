package rest

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/utils"
)

var (
	server *http.Server
)

func ListenAndServe() {
	r := mux.NewRouter()
	r.HandleFunc("/", renderRoot)
	r.HandleFunc("/api/v1alpha1/instances", CreateInstance).Methods("POST")
	r.HandleFunc("/api/v1alpha1/instances", ListInstances).Methods("GET")
	r.HandleFunc("/api/v1alpha1/instances/{id}", GetInstance)

	r.HandleFunc("/api/v1alpha1/jobs", ListJobs)
	r.HandleFunc("/api/v1alpha1/jobs/{id}", GetJob)

	server = &http.Server{
		Addr:    config.C.Bind,
		Handler: utils.LoggingMiddleware(r),
	}
	log.Println("Starting API server on", server.Addr)
	err := server.ListenAndServe()
	if err != http.ErrServerClosed {
		panic(err)
	}
}

func renderRoot(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"service": "rhyzome"})
}

func Shutdown() error {
	log.Println("Shutting down API server")
	return server.Shutdown(context.Background())
}
