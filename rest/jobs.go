package rest

import (
	"encoding/json"
	"net/http"

	"git.callpipe.com/entanglement.garden/rhyzome/jobs"
	"github.com/gorilla/mux"
)

func ListJobs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(jobs.All())
}

func GetJob(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	job := jobs.Get(mux.Vars(r)["id"])
	if job == nil {
		w.WriteHeader(404)
	}
	json.NewEncoder(w).Encode(job)
}
