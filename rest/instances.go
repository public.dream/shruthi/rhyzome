package rest

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"git.callpipe.com/entanglement.garden/rhyzome/instances"
	"git.callpipe.com/entanglement.garden/rhyzome/jobs"
)

func ListInstances(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	instanceList, err := instances.GetAll()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	json.NewEncoder(w).Encode(instanceList)
}

func GetInstance(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id, ok := mux.Vars(r)["id"]
	if !ok {
		w.WriteHeader(404)
		json.NewEncoder(w).Encode(map[string]string{"error": "instance not found"})
		return
	}

	instance, err := instances.FromDomainUUID(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	json.NewEncoder(w).Encode(instance)
}

func CreateInstance(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	instance := instances.GetDefaultInstance()
	json.NewDecoder(r.Body).Decode(&instance)
	// TODO: Validate the instance's properties
	j := jobs.NewJob("CreateInstance", instance.CreateAsJob)
	log.Printf("CreateInstance called, running as job %s", j.ID)
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(j)
}
