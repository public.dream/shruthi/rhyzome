package utils

import (
	"log"
	"net/http"
)

type ContextKey int

type wrapper struct {
	http.ResponseWriter
	written int
	status  int
}

const (
	InstanceDomainContextKey    ContextKey = 0
	InstanceDomainXMLContextKey ContextKey = 1
	LibvirtConnecKey            ContextKey = 2
)

func (w *wrapper) WriteHeader(code int) {
	w.status = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *wrapper) Write(b []byte) (int, error) {
	n, err := w.ResponseWriter.Write(b)
	w.written += n
	return n, err
}

// LoggingMiddleware is a middleware function that logs requests as they come in
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res := &wrapper{w, 0, 0}
		next.ServeHTTP(res, r)
		log.Printf("%s %s %s %d %d", r.RemoteAddr, r.Method, r.RequestURI, res.status, res.written)
	})
}
