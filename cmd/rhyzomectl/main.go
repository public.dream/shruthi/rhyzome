package main

import "git.callpipe.com/entanglement.garden/rhyzome/client/cli"

func main() {
	cli.Execute()
}
