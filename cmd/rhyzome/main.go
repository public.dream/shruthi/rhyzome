package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"git.callpipe.com/entanglement.garden/rhyzome/config"
	"git.callpipe.com/entanglement.garden/rhyzome/metadata"
	"git.callpipe.com/entanglement.garden/rhyzome/rest"
	"git.callpipe.com/entanglement.garden/rhyzome/vault"
)

var (
	signals = make(chan os.Signal, 1)
)

func main() {
	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)

	config.Load()
	vault.Load()

	go metadata.ListenAndServe()
	go rest.ListenAndServe()

	for {
		signal := <-signals
		log.Println("Received signal", signal)
		switch signal {
		case syscall.SIGHUP:
			config.Load()
		case syscall.SIGINT:
			if err := rest.Shutdown(); err != nil {
				log.Println("Error shutting down API server", err)
			}

			if err := metadata.Shutdown(); err != nil {
				log.Println("Error shutting down metadata server", err)
			}

			return
		}
	}
}
